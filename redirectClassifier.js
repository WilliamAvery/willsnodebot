const WillsNodeBot = require('./willsnodebot').botObject
//
// Bot - framework handles authentication,
// maxlag retries, transient network errors
//
const bot = new WillsNodeBot()

bot.initConfig({
    instance: 'live',
    'logging': false,
    dryrun: true,
    prompt: false,
    editlimit: 999999
})


const aliases = JSON.stringify(["R from scientific name", "Rsci", "Redirect from scientific name",
    "R from biological name", "Redirect from biological name",
    "Redirect from astronomical name", "R from scientific title",
    "Redirect from scientific title", "R from binomial name",
    "Redirect from binomial name", "R from species name",
    "Redirect from species name", "R to non-scientific name",
    "Redirect to non-scientific name", "R to non-scientific title",
    "Redirect to non-scientific title", "R from order", "Redirect from order",
    "R from biological member", "Redirect from biological member",
    "R to common name", "Redirect from nomen erratum", "R to common",
    "Redirect to common name", "R sci",])

let counters = {
    plant: 0,
    fungus: 0,
    insect: 0,
    fish: 0,
    spider: 0,
    reptile: 0,
    crustacean: 0,
    others: 0
}

bot.setCookie().then(() => {
    bot.wikiGeneratorStream({
        action: 'query',
        generator: 'categorymembers',
        gcmtitle: 'Category:Redirects from scientific names',
        gcmlimit: 5000,    // gcmendsortkeyprefix only seems to work in the absence of continuation
        gcmsort: 'sortkey',
        gcmstartsortkeyprefix: 'H',
        gcmendsortkeyprefix: 'I',
        prop: 'info',
        rvprop: ''
    })
        //.pipe(bot.filter(data => data.title > 'D' && data.title < 'Db')) // use gcmstartsortkeyprefix gcmendsortkeyprefix to get tranches
        /*     bot.wikiStream(
                {
                    action: 'query',
                    prop: 'info|revisions',
                    rvprop: 'timestamp|content',
                    titles: 'Acanthostracion polygonius|Acanthostracion quadricornis|Acanthosoma haemorrhoidale|Acanthophacelus reticulatus|Acanthopsetta nadeshnyi|Acanthopsetta|Acanthocephala femorata|Acanthocybium'
                }) */
        //
        // Get HTML for target of redirect, and instantiate DOM
        //
        .pipe(bot.domAdder(true))
        //
        // Get taxonomic information by running JQuery selectors on the target page
        //
        .pipe(bot.attributesFromDom('kingdom', 'table.infobox.biota tr td:contains(\'Kingdom:\') + td'))
        .pipe(bot.attributesFromDom('subphylum', 'table.infobox.biota tr td:contains(\'Subphylum:\') + td'))
        .pipe(bot.attributesFromDom('class', 'table.infobox.biota tr td:contains(\'Class:\') + td'))
        .pipe(bot.attributesFromDom('order', 'table.infobox.biota tr td:contains(\'Order:\') + td'))
        //
        // Determine what parameter should be applied in the redirect template 
        //
        .pipe(
            bot.runFunction((data, done, bot) => {

                if (data.kingdom == 'Plantae')
                    data.ptag = 'plant';
                if (data.kingdom == 'Fungi')
                    data.ptag = 'fungus';
                if (data.subphylum == 'Crustacea')
                    data.ptag = 'crustacean';
                if (data.class == 'Actinopterygii' || data.class == 'Achondrichthyes')
                    data.ptag = 'fish';
                if (data.order == 'Testudines' || data.order == 'Squamata' || data.order == 'Crocodilia')
                    data.ptag = 'reptile';
                if (data.class == 'Insecta')
                    data.ptag = 'insect';
                if (data.order == 'Araneae')
                    data.ptag = 'spider';
                if (data.ptag) {
                    counters[data.ptag]++
                }
                else
                    counters.others++
                done();
            })
        )
        // 
        // Drop items with no parameter value to be applied
        //
        .pipe(bot.filter(data => data.ptag))
        //
        // Add parameter value to relevant template
        //
        .pipe(bot.chunker(20))
        .pipe(bot.pageRetriever())
        .pipe(bot.exclusionFilter())   // Eliminate pages with bot exclusion
        .pipe(bot.mwparserfromhell((data) => {
            return (`
for template in wikicode.filter_templates():
   if template.name in ${aliases}:
      template.add(1, '${data.ptag}')`)
        }))
        .pipe(
            bot.runFunction((d, done, b) => {
                d.page.text = d.page.revisions[0]['*']
                d.page.editsummary = `Adding parameter ${d.ptag}. See [[WP:Bots/Requests for approval/William Avery Bot 2|BRFA]]`
                b.processPage(d.page).then(done, e => { console.error(e.toString()); done() })
            })
        )
        .on('data', r => { console.log(r.title + ' ' + r.order + ' ' + r.ptag) })
        .on('finish', () => console.log(JSON.stringify(counters)))
})
