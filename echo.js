exports.run = function(page, options) {
    return new Promise((accept, reject) => {
        console.log('echo: ' + page.title + ' timestamp: ' + page.timestamp)
        console.log('editsummary: ' + page.editsummary)
        console.log('=======================================================')
        console.log(page.text)
        console.log('=======================================================')
        accept(page)
    })
};
