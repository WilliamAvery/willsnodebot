"use strict"
var gulp = require('gulp');
var bot = require('./willsnodebot.js').botObject;
const commandLineArgs = require('command-line-args')

const options = {
    config: '~/.botConfig.js',
    instance: 'test'
}

const optionDefinitions = [{
    name: 'config',
    alias: 'f',
    type: String,
    defaultValue: options.config
}, {
    name: 'logging',
    alias: 'l',
    type: Boolean,
    defaultValue: false
}, {
    name: 'prompt',
    alias: 'p',
    type: Boolean,
    defaultValue: false
}, {
    name: 'editlimit',
    type: Number,
    defaultValue: Number.MAX_SAFE_INTEGER
}, {
    name: 'dryrun',
    type: Boolean,
    defaultValue: false
}, {
    name: 'create',
    type: Boolean,
    defaultValue: false
}, {
    name: 'instance',
    alias: 'i',
    type: String,
    defaultValue: options.instance
}]


gulp.task('clibot', function() {
    const b = new bot()
    b.run()
});
gulp.task('echobot', function() {
    let currOptions = commandLineArgs(optionDefinitions, {
        partial: true
    })
    Object.assign(currOptions, {titles: 'Main_page'})
    const b = new bot()
    b.run(currOptions
    )
});

gulp.task('movenongastropods', function() {

    optionDefinitions.push({
        name: 'year',
        alias: 'y',
        type: String,
    })
    let currOptions = commandLineArgs(optionDefinitions, {
        partial: true
    })
    Object.assign(currOptions, {
        petscan: {
            categories: `Animals described in ${currOptions.year}|0\nMolluscs|10`,
            negcats: "Gastropods|10"
        },
        module: 'regexp.js',
        modparams: {
            regexp: "(\\[\\[\\s*Category\\:\\s*)(Animals)(\\s*described\\s*in\\s*\\d\\d\\d\\d\\s*\\|?[^\\]]*\\]\\])",
            subst: "$1Molluscs$3",
            summary: `Removing [[:Category:Animals described in ${currOptions.year}]] and adding [[:Category: Molluscs described in ${currOptions.year}]]. See [[WP:Bots/Requests for approval/William Avery Bot|BRFA]]`
        }
    })
    const b = new bot()
    b.run(currOptions)
});

gulp.task('movegastropods_fromanimals', function() {

    optionDefinitions.push({
        name: 'year',
        alias: 'y',
        type: String,
    })
    let currOptions = commandLineArgs(optionDefinitions, {
        partial: true
    })
    Object.assign(currOptions, {
        petscan: {
            categories: `Animals described in ${currOptions.year}|0\nGastropods|10`
        },
        module: 'regexp.js',
        modparams: {
            regexp: "(\\[\\[\\s*Category\\:\\s*)(Animals|Molluscs)(\\s*described\\s*in\\s*\\d\\d\\d\\d\\s*\\|?[^\\]]*\\]\\])",
            subst: "$1Gastropods$3",
            summary: `Removing [[:Category:Animals described in ${currOptions.year}]] and adding [[:Category: Gastropods described in ${currOptions.year}]]. See [[WP:Bots/Requests for approval/William Avery Bot|BRFA]]`
        }
    })
    const b = new bot()
    b.run(currOptions)
});

gulp.task('movegastropods_frommolluscs', function() {

    optionDefinitions.push({
        name: 'year',
        alias: 'y',
        type: String,
    })
    let currOptions = commandLineArgs(optionDefinitions, {
        partial: true
    })
    Object.assign(currOptions, {
        petscan: {
            categories: `Molluscs described in ${currOptions.year}|0\nGastropods|10`
        },
        module: 'regexp.js',
        modparams: {
            regexp: "(\\[\\[\\s*Category\\:\\s*)(Animals|Molluscs)(\\s*described\\s*in\\s*\\d\\d\\d\\d\\s*\\|?[^\\]]*\\]\\])",
            subst: "$1Gastropods$3",
            summary: `Removing [[:Category:Molluscs described in ${currOptions.year}]] and adding [[:Category: Gastropods described in ${currOptions.year}]]. See [[WP:Bots/Requests for approval/William Avery Bot|BRFA]]`
        }
    })
    const b = new bot()
    b.run(currOptions)
});
gulp.task('simpleregexp', function() {

    let currOptions = commandLineArgs(optionDefinitions, {
        partial: true
    })
    Object.assign(currOptions, {
        titles: process.env.titles,
        module: 'regexp.js',
        modparams: {
            regexp: process.env.regexp,
            subst: process.env.subst,
            options: options in process.env ? process.env.options : '',
            summary: process.env.summary
        }
    })
    const b = new bot()
    b.run(currOptions)
});


gulp.task('desyncedByWikiProject', function() {
    let currOptions = commandLineArgs(optionDefinitions, {
        partial: true
    })
    const projectTree = `
==Wikipedia:WikiProject Animals
===Molluscs
====Wikipedia:WikiProject Bivalves
====Wikipedia:WikiProject Gastropods
====Wikipedia:WikiProject Cephalopods
===Wikipedia:WikiProject Arthropods
====Wikipedia:WikiProject Insects
=====Wikipedia:WikiProject Lepidoptera
=====Wikipedia:WikiProject Beetles
====Wikipedia:WikiProject Spiders
===Vertebrates
====Wikipedia:WikiProject Bivalves
====Wikipedia:WikiProject Fishes
=====Wikipedia:WikiProject Aquarium Fishes
=====Wikipedia:WikiProject Sharks
====Wikipedia:WikiProject Amphibians and Reptiles
=====Wikipedia:WikiProject Dinosaurs
=====Wikipedia:WikiProject Turtles
====Wikipedia:WikiProject Birds
=====Wikipedia:WikiProject Birds/Domestic pigeon task force
=====Wikipedia:WikiProject Poultry
====Wikipedia:WikiProject Mammals
=====Wikipedia:WikiProject Monotremes and Marsupials
=====Wikipedia:WikiProject Cetaceans
=====Wikipedia:WikiProject Primates
=====Wikipedia:WikiProject Mammals/Bats Task Force
=====Wikipedia:WikiProject Rodents
=====Wikipedia:WikiProject Cats
=====Wikipedia:WikiProject Dogs
=====Wikipedia:WikiProject Equine
==Wikipedia:WikiProject Plants
===Wikipedia:WikiProject Banksia
===Wikipedia:WikiProject Carnivorous plants
===Wikipedia:WikiProject Horticulture and Gardening
===Wikipedia:WikiProject Pteridophytes
==Wikipedia:WikiProject Microbiology
===Wikipedia:WikiProject Prokaryotes and Protists
===Wikipedia:WikiProject Viruses
==Wikipedia:WikiProject Fungi
==Wikipedia:WikiProject Algae
==Wikipedia:WikiProject Marine life
==Wikipedia:WikiProject Palaeontology
==Wikipedia:WikiProject Australian biota
==Wikipedia:WikiProject Biota of Great Britain and Ireland
`
    let newPage = '';

    const prefix = "User:William Avery/Desynced_taxonbars_by_WikiProject/"
    var pageList = projectTree.match(/WikiProject .+\n/g).map((matc) => {
	    let retval = ''
        console.log(matc.toString())
        retval = prefix + matc.toString().substr(12).replace('\n', '')
	return retval
    }).join('|')
    console.log(pageList)
    Object.assign(currOptions, {
        config: './Sample.botConfig.js',
        module: 'wikiProjectDesynced.js',
        titles: pageList,
	modparams: {basepage: prefix}
    })
    const b = new bot()
    b.run(currOptions)
})
