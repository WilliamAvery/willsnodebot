const Readable = require('stream').Readable
const Transform = require('stream').Transform
const Duplex = require('stream').Duplex
const bot = require('./willsnodebot').botObject


//
// Bot - framework handles authentication,
// maxlag retries, transient network errors
//
const myBot = new bot()

myBot.initConfig({
    instance: 'data',
    'logging': false
})
myBot.setCookie().then(() => {
    //
    // Run PetScan
    //
    new petScan({
            categories: 'Taxonbars_desynced_from_Wikidata',
            sortby: 'title'
        })
        //
        // Get HTML for Wikipedia page
        // and attach DOM obj with jQuery
        //
        .pipe(new domAdder(false))
        //
        // Get wikidata key from taxonbar
        //
        .pipe(new attributesFromDom("wikidatakey", "div.navbox a[title^=wikidata].extiw:first"))
        .pipe(new filter((d) => d.wikidatakey))
        //
        // Check for currently linked entities
        //
        .pipe(new taxonbarEntityRetrieval())
        .pipe(new filter((d) => d.redirection))
        .on('data', (obj) => {
            console.log('Redirected taxonbar for ' + JSON.stringify(obj))
        })
})

function petScan(params) {
    let limit = 1000;
    const queryRun = myBot.petScan(params)
    const stream = new Readable({
        objectMode: true,
        read() {
            queryRun.then((r) => {
                stream.push(r.length && limit-- ? {
                    title: r.shift().title
                } : null)
            })
        }
    })
    return stream
}



function domAdder(raw) {
    return new Transform({
        objectMode: true,
        highWaterMark: 1,
        transform: (data, _, done) => {
	    //console.log("title=" + data.title)
            myBot.getHTML(data.title).then((dom) => {
                if (!raw) delete(dom.raw);
                done(null, {
                    title: data.title,
                    html: dom
                })
            })
        }
    })
}

function attributesFromDom(attr, selector, deletedom) {
    return new Transform({
        objectMode: true,
        highWaterMark: 1,
        transform: (data, _, done) => {
            const sOut = data.html.$(selector)
            if (sOut.length)
                data[attr] = sOut.text()
		//console.log(JSON.stringify(data) +  " = " + data[attr])
            delete(data.html.dom) // Big. Get rid.
            done(null,
                data
            )
        }
    })
}


function taxonbarEntityRetrieval() {
    let readfunc
    const _dup = new Duplex({
        objectMode: true,
        highWaterMark: 1,
        read: () => {
            if (readfunc) {
                readfunc()
                readfunc = null
            }
        },
        write: (data, _, done) => {
	    console.log("ids=" + data.wikidatakey + " title=" + data.title)
            myBot.dataRequest({
                action: 'wbgetentities',
                sites: 'enwiki',
		redirects: "yes",
                ids: data.wikidatakey
            }).then((results) => {
                ents = results.entities
                Object.keys(ents).forEach((dk) => {
                    const id = ents[dk].id
                    if (id == data.wikidatakey) {
 
                        // console.log("Found " + id + " for " + data.wikidatakey)
                    }
                    else {
			console.log("Got " + id + " for " + data.wikidatakey)
                        data.redirection = id
                    }
                })
                    if (!_dup.push(data))
                        readfunc = done
                if (!readfunc)
                    done()
            })
        }
    })
    return _dup
}


function filter(executable) {
    let readFunc = []
    let writeFunc = []
    const _dup = new Duplex({
        objectMode: true,
        highWaterMark: 1,
        read: () => {
            if (writeFunc.length) {
                writeFunc.pop()()
            } else {
                readFunc.push((data) => {
                    _dup.push(data)
                })
            }
        },
        write: (data, _, done) => {
            if (executable(data))
                if (readFunc.length) {
                    readFunc.pop()(data)
                    done()
                } else {
                    writeFunc.push(() => {
                        _dup.push(data);
                        done()
                    })
                }
            else
                done()
        }
    })
    return _dup
}
