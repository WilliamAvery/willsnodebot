# willsnodebot

A bot for editing MediaWiki wiki pages

This software is in a primordial state. It is not ready for use by anyone other than the author. 

It does, or did, things as: 

* https://www.wikidata.org/wiki/User:William_Avery_Bot
* https://en.wikipedia.org/wiki/User:William_Avery_Bot

## To run on Linux
```
git clone https://WilliamAvery@bitbucket.org/WilliamAvery/willsnodebot.git

cd willsnodebot
npm build
cd ..
cp willsnodebot/Sample.botConfig.js ~/.botConfig.js
```

Edit the ~/.botConfig.js

## Sample commands
### On Linux / Unix shell:
```
gulp clibot --instance live --titles "User:William_Avery_Bot/test" --module regexp.js --modparams '{"regexp":"(\\[\\[\\s*\\:\\s*Category\\:\\s*)(Animals)(\\s*described\\s*in\\s*2025\\s*\\]\\])", "subst":"$1Molluscs$3", "flags":"g"}'
```

```
gulp clibot -s '{"categories":"Astroblepus"}'
```
```
gulp clibot --instance live -s '{"categories":"Animals described in 1880\nMollusca|6","negcats":"Gastropod taxa|5"}'
```
### On Windows: 
```
gulp clibot.js --instance live -t "User:William Avery Bot/Test1|User:William Avery Bot/Test2|User:William Avery Bot/Test3" -m regexp.js --modparams "{""regexp"":""Another rigorous test"",""subst"":"""",""flags"":""g"",""summary"": ""Clean""}"
```

## Gulp tasks for trial on English Wikipedia
```
gulp movegastropods_fromanimals --instance live --year 1880  --editlimit 1  --dryrun
```
```
gulp movegastropods_frommolluscs -i live -y 1880  --editlimit 10 
```
```
gulp movenongastropods -i live  --editlimit 1 -y 1913
```
## Docker 
```
docker run -e regexp="$" -e subst="Testing" -e titles="Main Page" -e summary="Test edit summary" -e botpasswd="******" willaverydotnet/willsnodebot gulp simpleregexp --config=./Sample.botConfig.js
```
