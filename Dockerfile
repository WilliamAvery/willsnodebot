FROM node:6.9.4

# Create app directory
RUN mkdir -p /usr/src/willsnodebot
WORKDIR /usr/src/willsnodebot

# Install app dependencies
COPY package.json /usr/src/willsnodebot/
RUN npm install
RUN npm install gulp -g 

# Bundle app source
COPY . /usr/src/willsnodebot