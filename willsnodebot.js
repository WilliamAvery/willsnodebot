"use strict";

const os = require('os');
const fs = require('fs');
const url = require('url');
const request = require('request');
const querystring = require('querystring');
const commandLineArgs = require('command-line-args')
const cookie = require('cookie');
const prompt = require('prompt')
const jsdom = require("jsdom");
const {
    JSDOM
} = jsdom;

function allowBots(text, user) {
    if (!new RegExp("\\{\\{\\s*(nobots|bots[^}]*)\\s*\\}\\}", "i").test(text)) return true;
    return (new RegExp("\\{\\{\\s*bots\\s*\\|\\s*deny\\s*=\\s*([^}]*,\\s*)*" + user.replace(/([\(\)\*\+\?\.\-\:\!\=\/\^\$])/g, "\\$1") + "\\s*(?=[,\\}])[^}]*\\s*\\}\\}", "i").test(text)) ? false : new RegExp("\\{\\{\\s*((?!nobots)|bots(\\s*\\|\\s*allow\\s*=\\s*((?!none)|([^}]*,\\s*)*" + user.replace(/([\(\)\*\+\?\.\-\:\!\=\/\^\$])/g, "\\$1") + "\\s*(?=[,\\}])[^}]*|all))?|bots\\s*\\|\\s*deny\\s*=\\s*(?!all)[^}]*|bots\\s*\\|\\s*optout=(?!all)[^}]*)\\s*\\}\\}", "i").test(text);
}
class botObject {
    constructor() {
        console.log(`Bot starting at ${new Date().toString()}...`);
        this.actionticker = {};
        this.edittoken = {};
        this.json_headers = {
            'content-type': 'application/json;charset=utf-8',
            accept: 'application:json'
        };
        this.nextRetrieveTime = Date.now()
        this.flushMarker = { flush: 'The data pipe was flushed before end of run or hibernation' }

    }
    getEditToken(urikey, action, existingtoken, meta) {
        return new Promise((resolve, reject) => {
            if (meta == 'tokens' || existingtoken)
                resolve({});
            else if (action == 'login' || ((action == 'edit' || action == 'wbsetsitelink') && !this.edittoken[urikey]))
                this.baseRequest(urikey, {
                    action: 'query',
                    meta: 'tokens',
                    type: (action == 'login' ? action : 'csrf')
                }).then((respObj) => {
                    if (this.options.logging)
                        console.log('Tokens: ' + JSON.stringify(respObj));
                    if (respObj.query.tokens.csrftoken) {
                        this.edittoken[urikey] = respObj.query.tokens.csrftoken;
                        resolve({
                            token: this.edittoken[urikey]
                        });
                    }
                    else
                        resolve({
                            lgtoken: respObj.query.tokens.logintoken
                        });
                }).catch(reject);
            else if (action == 'edit' || action == 'wbsetsitelink')
                resolve({
                    token: this.edittoken[urikey]
                });
            else
                resolve({});
        });
    }
    formBreaksActionLimits(form, reject) {
        const curAction = form.action;
        this.actionticker[curAction] = curAction in this.actionticker ? this.actionticker[curAction] + 1 : 1;
        if (this.options.logging)
            console.log(`Action: ${curAction}  ticker: ${this.actionticker[curAction]}  limit: ${this.actionlimits[curAction]}`);
        if (curAction in this.actionlimits && this.actionticker[curAction] > this.actionlimits[curAction]) {
            if ('lgpasswd' in form)
                form.lgpasswd = '******';
            if ('text' in form)
                form.text = '[Text removed]';
            const err = new Error(`Action limit ${this.actionlimits[curAction]} reached on ${curAction}. Did not process form: ${JSON.stringify(form)}`);
            reject(err)
            return true;
        }
        return false;
    }
    request(form, delay) {
        return this.baseRequest('ApiUrl', form, delay);
    }
    dataRequest(form, delay) {
        return this.baseRequest('DataUrl', form, delay);
    }
    baseRequest(UriKey, form, delay) {
        if (!delay)
            delay = 0;
        else
            if (this.options.logging || 1)
                console.log(`Request delay=${delay}`);
        const uri = this[UriKey];
        return new Promise((resolve, reject) => {
            if (this.formBreaksActionLimits(form, reject))
                return;
            let mwReq = {
                form: form,
                uri: uri,
                headers: this.json_headers,
                method: 'POST'
            };
            setTimeout(() => {
                if (!(form.action == 'login' || form.action == 'query' && form.meta == 'tokens'))
                    form.assert = this.instance.assert;
                if (form.action == 'edit' && !form.maxlag) // Allow override of maxlag
                    form.maxlag = this.maxlag;
                this.getEditToken(UriKey, form.action, form.token, form.meta).then((tokenObj) => {
                    Object.assign(form, tokenObj);
                    if (this.options.logging)
                        console.log(JSON.stringify(mwReq));
                    request(mwReq, (mwError, mwResponse, mwBody) => {
                        if (mwError)
                            if (mwError == 'Error: read ECONNRESET') {
                                console.log('Retrying on read ECONNRESET error');
                                setTimeout(() => {
                                    this.baseRequest(UriKey, form).then(resolve, reject);
                                }, 10000);
                            }
                            else
                                reject(new Error(`Failed on MW API request to ${uri}: "${mwError}"`));
                        else if (mwResponse.statusCode !== 200)
                            reject(new Error(`Status on MW API request: ${JSON.stringify(mwReq)} \n${mwResponse.statusCode}`));
                        else {
                            if (this.options.logging) {
                                console.log('Response body:\n' + mwBody);
                            }
                            if (mwResponse.headers['set-cookie'])
                                this.setCookieJar(mwResponse.headers['set-cookie']); // MW session cookie
                            if (mwResponse.headers['retry-after'])
                                try {
                                    this.maxlagwait = parseInt(mwResponse.headers['retry-after']) * 1000;
                                    console.log(`Retry after (ms): ${this.maxlagwait}`);
                                }
                                catch (e) {
                                    this.maxlagwait = 5000;
                                    console.log('Could not parse retry-after: ' + e.message);
                                }
                            const bodyObj = JSON.parse(mwBody);
                            if (!bodyObj.error)
                                resolve(bodyObj);
                            else {
                                console.error(`Error: ${bodyObj.error.code} - ${bodyObj.error.info}`);
                                if ((this.cnf.settings.continuable_errors && this.cnf.settings.continuable_errors.indexOf(bodyObj.error.code) != -1) &&
                                    (!this.module.continuable_errors || this.module.continuable_errors.indexOf(bodyObj.error.code) != -1)) {
                                    if (bodyObj.error.code == 'maxlag') {
                                        // If testing maxlag, tend back towards config value
                                        if (this.maxlag < this.cnf.settings.maxlag) {
                                            this.maxlag += Math.ceil((this.cnf.settings.maxlag - this.maxlag) / 2);
                                            console.log(`maxlag adjusted to ${this.maxlag}`);
                                            form.maxlag = this.maxlag;
                                        }
                                        //
                                        // retries on maxlag are by recursion
                                        //
                                        setTimeout(() => {
                                            this.baseRequest(UriKey, form).then(resolve);
                                        }, this.maxlagwait);
                                    }
                                    else
                                        resolve(bodyObj);
                                }
                                else {
                                    reject(new Error('Failed: ' + bodyObj.error.info)); // Network or DNS failure
                                }
                            }
                        }
                    });
                }).catch(reject);
            }, delay)
        });
    }
    getCookies() {
        return Object.keys(this.cookieJar[this.options.instance]).reduce((cookiestr, key) => {
            let basicVal = this.cookieJar[this.options.instance][key];
            const scIndex = basicVal.indexOf(';');
            basicVal = scIndex > 0 ? basicVal.substr(0, scIndex) : basicVal;
            cookiestr += (cookiestr ? ';' : '') + basicVal;
            return cookiestr;
        }, '');
    }
    setCookieJar(setcookies) {
        const oldCookies = JSON.stringify(this.cookieJar[this.options.instance]);
        if (this.options.logging)
            console.log(`Writing cookiejar ${this.cookieJar}`);
        if (!this.cookieJar[this.options.instance])
            this.cookieJar[this.options.instance] = {};
        setcookies.forEach((setcookie) => {
            const eqIndex = setcookie.indexOf('=');
            const cookiename = eqIndex > 0 ? setcookie.substr(0, eqIndex) : setcookie;
            let thisCookie = cookie.parse(setcookie);
            if (this.options.logging)
                console.log('Set cookie:' + setcookie);
            if (thisCookie.expires || thisCookie.Expires || true) {
                if (this.options.logging)
                    console.log('Saved cookie:' + cookiename);
                this.cookieJar[this.options.instance][cookiename] = setcookie;
            }
        });
        this.json_headers.cookie = this.getCookies();
        if ('cookieJar' in this.cnf.settings && this.cnf.settings.cookieJar.trim())
            if (oldCookies != JSON.stringify(this.cookieJar[this.options.instance]))
                fs.writeFileSync(this.cnf.settings.cookieJar, JSON.stringify(this.cookieJar), {
                    mode: '0600'
                });
    }
    initConfig(params) {
        return new Promise((resolve, reject) => {
            this.options = {
                config: '~/.botConfig.js',
                instance: 'test',
                module: './echo.js',
                modparams: '{}',
                "actionlimits": '{}'
            };
            if (params)
                Object.assign(this.options, params);
            else {
                this.optionDefinitions = [{
                    name: 'config',
                    alias: 'f',
                    type: String,
                    defaultValue: this.options.config
                }, {
                    name: 'titles',
                    alias: 't',
                    type: String,
                    defaultValue: ''
                }, {
                    name: 'titles2',
                    alias: 'w',
                    type: String,
                    defaultValue: ''
                }, {
                    name: 'logging',
                    alias: 'l',
                    type: Boolean,
                    defaultValue: false
                }, {
                    name: 'prompt',
                    alias: 'p',
                    type: Boolean,
                    defaultValue: false
                }, {
                    name: 'petscan',
                    alias: 's',
                    type: String,
                    defaultValue: ''
                }, {
                    name: 'module',
                    alias: 'm',
                    type: String,
                    defaultValue: this.options.module
                }, {
                    name: 'modparams',
                    alias: 'n',
                    type: String,
                    defaultValue: this.options.modparams
                }, {
                    name: 'dryrun',
                    type: Boolean,
                    defaultValue: false
                }, {
                    name: 'create',
                    type: Boolean,
                    defaultValue: false
                }, {
                    name: 'actionlimits',
                    alias: 'a',
                    type: String,
                    defaultValue: this.options.actionlimits
                }, {
                    name: 'editlimit',
                    type: Number,
                    defaultValue: Number.MAX_SAFE_INTEGER
                }, {
                    name: 'instance',
                    alias: 'i',
                    type: String,
                    defaultValue: this.options.instance
                }, {
                    name: 'gulpcommand',
                    type: String,
                    defaultValue: '',
                    defaultOption: true
                }];
                this.options = commandLineArgs(this.optionDefinitions);
            }
            this.options.config = this.options.config.replace(/^~/, os.homedir());
            let configURL;
            try {
                configURL = url.parse(this.options.config).href;
            }
            catch (e) {
                console.log(e.message);
                console.log(`Could not process configuration file name "${this.options.config}" as URL`);
                configURL = this.options.config;
            }
            const confStat = fs.statSync(configURL);
            if (!confStat) {
                reject(new Error(`Configuration file "${this.options.config}" does not exist`));
                return;
            }
            try {
                this.cnf = require(configURL);
            }
            catch (e) {
                reject(new Error(`Problem with config ${configURL}: ${e.message}`));
                return;
            }
            if (this.options.logging)
                console.log('Config: ' + JSON.stringify(configURL));
            if (!this.cnf.settings) {
                reject(new Error(`Configuration file "${this.options.config}" does not export the expected "settings"`));
                return;
            }
            if (!this.cnf.settings.instances)
                reject(new Error(`The settings in configuration file "${this.options.config}" contain no instances`));
            if (process.env.botinstance)
                this.instancekey = process.env.botinstance;
            else
                this.instancekey = this.options.instance;
            if (!(this.instancekey in this.cnf.settings.instances)) {
                reject(new Error(`No instance ${this.instancekey} in config`));
                return;
            }
            this.instance = this.cnf.settings.instances[this.instancekey];
            if (this.options.logging)
                console.log('Instance: ' + JSON.stringify(this.instance));
            console.log('Running against ' + this.instance.Description);
            if (this.instance.shutoffpage == ':')
                this.instance.shutoffpage = 'User talk:' + this.instance.lgname + '/Shutdown';
            if (this.instance.shutoffpage)
                console.log('Shutoff page ' + this.instance.shutoffpage);
            this.ApiUrl = this.instance.ApiUrl + '?' + querystring.stringify({
                format: 'json'
            });
            if (this.instance.DataUrl)
                this.DataUrl = this.instance.DataUrl + '?' + querystring.stringify({
                    format: 'json'
                });
            this.maxlag = this.cnf.settings.maxlag;
            this.maxlagwait = 5000;
            this.module = require('./' + this.options.module);
            this.lastedit = Date.now() - this.instance.editinterval;
            this.modparams = this.optObjFromString('modparams', reject);
            this.actionlimits = this.optObjFromString('actionlimits', reject);
            if (this.options.editlimit != Number.MAX_SAFE_INTEGER)
                this.actionlimits.edit = this.options.editlimit;
            if (this.options.logging) {
                console.log('Options: ' + JSON.stringify(this.options));
            }
            resolve();
        });
    }
    optObjFromString(option, reject) {
        let opt = this.options[option];
        if (typeof opt == 'string') // From command line
            try {
                this.options[option] = JSON.parse(opt);
            }
            catch (e) {
                reject(new Error(`Could not parse option ${option} value ${opt} : ${e.message}`));
            }
        return this.options[option];
    }
    updPrompt(title) {
        return new Promise((resolve, reject) => {
            if (this.options.prompt) {
                console.log(`Halted before update of ${title}`);
                console.log('Input "skip", "abort", "maxlag=nnn", "noprompt" or press Return to continue');
                prompt.start();
                prompt.get({
                    properties: {
                        Options: {}
                    }
                }, (err, data) => {
                    const retObj = {};
                    data.Options.split(';').forEach((frag) => {
                        const eqIndex = frag.indexOf('=');
                        let propName = frag;
                        let propVal = true;
                        if (eqIndex > 0) {
                            propName = frag.substr(0, eqIndex).trim();
                            propVal = frag.substr(eqIndex + 1).trim();
                        }
                        if (propName) {
                            console.log(`${propName}=${propVal}`);
                            retObj[propName] = propVal;
                        }
                    });
                    resolve(retObj);
                });
            }
            else
                resolve({});
        });
    }
    getPasswd() {
        return new Promise((resolve, reject) => {
            if (process.env.botpasswd)
                resolve(process.env.botpasswd);
            else {
                console.log(`Password required for ${this.instance.lgname}`);
                prompt.start();
                prompt.get({
                    properties: {
                        password: {
                            hidden: true
                        }
                    }
                }, (err, data) => {
                    resolve(data.password);
                });
            }
        });
    }
    setCookie() {
        return new Promise((resolve, reject) => {
            this.cookieJar = {};
            let contents = '';
            if ('cookieJar' in this.cnf.settings && this.cnf.settings.cookieJar.trim()) {
                try {
                    contents = fs.readFileSync(this.cnf.settings.cookieJar, 'utf8');
                    if (this.options.logging)
                        console.log(`Cookie jar=${contents}`);
                }
                catch (ENOENT) {
                    console.log(`No cookie jar: ${this.cnf.settings.cookieJar}`);
                }
                try {
                    this.cookieJar = JSON.parse(contents);
                }
                catch (e) {
                    console.log('Could not parse cookie jar contents. Resetting.');
                }
            }
            if (contents == '' || !this.cookieJar[this.options.instance] || Object.keys(this.cookieJar[this.options.instance]).length < 3) {
                //
                // Prompt for password, then get cookie
                //
                this.getPasswd().then((passwd) => {
                    if (this.options.logging)
                        console.log('About to request');
                    this.request({
                        action: 'login',
                        lgname: this.instance.lgname,
                        lgpassword: passwd
                    }).then((lresp) => {
                        if (lresp.login.result != 'Success') {
                            reject(new Error(lresp.login.reason));
                        }
                        else {
                            if (this.DataUrl)
                                this.dataRequest({
                                    action: 'login',
                                    lgname: this.instance.lgname,
                                    lgpassword: passwd
                                }).then((lresp) => {
                                    if (lresp.login.result != 'Success') {
                                        reject(new Error(JSON.stringify(lresp.login)));
                                    }
                                    else {
                                        resolve();
                                    }
                                });
                            else
                                resolve();
                        }
                    }).catch(reject);
                }).catch(reject);
            }
            else { // Existing cookie
                //
                // Put it in the JSON request header
                //
                this.json_headers.cookie = this.getCookies();
                if (this.options.logging)
                    console.log(`Cookies from jar: ${this.json_headers.cookie}`);
                resolve();
            }
        });
    }
    getHTML(title) {
        return new Promise((resolve, reject) => {
            const uri = this.instance.BaseUrl + title;
            const escuri = new url.URL(uri);
            let now = new Date().getTime()
            let wait = this.nextRetrieveTime - now
            if (now > this.nextRetrieveTime) {
                this.nextRetrieveTime = now
                wait = 0
            }
            this.nextRetrieveTime += 1000
            if (this.options.logging)
                console.log('next retrieve: ' + new Date(this.nextRetrieveTime).toString() + ' wait:' + wait.toString())
            setTimeout(() => {
                if (this.options.logging)
                    console.log(`Requesting: ${uri}`);
                request({
                    uri: escuri.href
                }, (htmErr, htmResponse, htmBody) => {
                    if (htmErr)
                        reject(new Error(`Error on ${uri} - ${htmErr}`));
                    else {
                        const dom = new JSDOM(htmBody);
                        const $ = require('jquery')(dom.window);
                        resolve({ raw: htmBody, dom: dom, '$': $ });
                    }
                });
            }, wait)
        });
    }
    petScan(psParams) {
        return new Promise((resolve, reject) => {
            if (this.options.logging)
                console.log('petscan params:' + JSON.stringify(psParams));
            const psForm = {
                active_tab: 'tab_categories',
                categories: '',
                cb_labels_any_l: '1',
                cb_labels_no_l: '1',
                cb_labels_yes_l: '1',
                combination: 'subset',
                common_wiki: 'auto',
                depth: '0',
                doit: 'Do+it!',
                format: 'json',
                interface_language: 'en',
                language: 'en',
                min_redlink_count: '1',
                'ns[0]': '1',
                output_compatability: 'catscan',
                project: 'wikipedia',
                show_redirects: 'both',
                subpage_filter: 'either',
                wikidata_item: 'no',
                wpiu: 'any'
            };
            Object.assign(psForm, psParams);
            if (this.options.logging)
                console.log("Submitting petscan");
            request({
                method: 'POST',
                uri: this.cnf.settings.petscanUri,
                form: psForm
            }, (petErr, petResponse, petBody) => {
                if (this.options.logging)
                    console.log('petscan body:' + petBody);
                if (petErr)
                    reject(new Error(`Petscan error on ${this.cnf.settings.petscanUri} - ${petErr}`));
                else
                    if (petResponse.statusCode != 200)
                        reject(new Error(`Petscan HTTP error ${petResponse.statusCode} on ${this.cnf.settings.petscanUri}`));
                    else {
                        try {
                            const jsonBody = JSON.parse(petBody);
                            const resArray = jsonBody['*'][0]['a']['*'];
                            if (this.options.logging)
                                console.log('petscan returned:' + JSON.stringify(resArray));
                            resolve(resArray);
                        } catch (error) {
                            console.log(petBody)
                            reject(error)
                        }

                    }
            });
        });
    }
    setTitleTranches() {
        return new Promise((resolve, reject) => {
            this.titleTranches = [];
            //
            // Lists of pages passed as command line parameters
            //
            if (this.options.titles)
                this.titleTranches.push(this.options.titles);
            if (this.options.titles2)
                this.titleTranches.push(this.options.titles2);
            if (this.options.logging)
                console.log("Command line tranches: " + JSON.stringify(this.titleTranches));
            //
            // Pull a list from PetScan if parameters were provided
            //
            if (this.options.petscan) {
                let psParams;
                if (typeof this.options.petscan == 'string') // From command line
                    try {
                        psParams = JSON.parse(this.options.petscan);
                        console.log("petscan parameters:" + JSON.stringify(psParams));
                    }
                    catch (e) {
                        console.error('Failed parsing petscan parameters');
                        throw e;
                    }
                else
                    psParams = this.options.petscan;
                this.petScan(psParams).then((resArray) => {
                    for (let startSlice = 0; startSlice < resArray.length; startSlice += this.instance.tranchesize) {
                        this.titleTranches.push(resArray.slice(startSlice, startSlice + this.instance.tranchesize).map(result => result.title).join('|'));
                    }
                    resolve();
                }).catch(reject);
            }
            else // No PetScan
                resolve();
        });
    }
    printPage(page) {
        console.log(`
* DRY RUN OUTPUT 
title: ${page.title}
basetimestamp: ${page.timestamp},
summary: ${page.editsummary},
text: -------------------------------------------->
${page.text}
<--------------------------------------------------
`);
    }
    //
    // Process an individual set of retrieved page details
    //
    processPage(page) {
        return new Promise((resolve, reject) => {
            //
            // Invoke specified module
            //
            //
            const services = {
                dataRequest: (params) => {
                    return this.dataRequest(params);
                },
                getHTML: (title) => {
                    return this.getHTML(title);
                },
                petscan: (psParams) => {
                    return this.petScan(psParams);
                },
                IsoToPet: (isoDate) => {
                    return isoDate.replace(/[: -]/g, '');
                },
                PetToIso: (petDate) => {
                    let retVal = petDate;
                    if (petDate.length == 14) {
                        const year = petDate.substr(0, 4);
                        const month = petDate.substr(4, 2);
                        const day = petDate.substr(6, 2);
                        const hour = petDate.substr(8, 2);
                        const min = petDate.substr(10, 2);
                        const sec = petDate.substr(12, 2);
                        retVal = year + '-' + month + '-' + day + ' ' + hour + ':' + min + ':' + sec;
                    }
                    return retVal;
                },
                PetDate: (pDate) => {
                    console.log(pDate.toString());
                    return pDate.getFullYear().toString() +
                        ('0' + (pDate.getMonth() + 1).toString()).slice(-2) +
                        ('0' + (pDate.getDate()).toString()).slice(-2) +
                        ('0' + (pDate.getHours()).toString()).slice(-2) +
                        ('0' + (pDate.getMinutes()).toString()).slice(-2) +
                        ('0' + (pDate.getSeconds()).toString()).slice(-2);
                }
            };
            this.module.run(page, this.modparams, services).then((page) => {
                //
                // Page summary is used as flag to indicate update required
                //
                if (page.editsummary) {
                    if (this.options.logging)
                        console.log('Summary: ' + page.editsummary);
                    //
                    // calculate wait required to throttle edit rate
                    //
                    const dateNow = Date.now();
                    let timeToWait = this.instance.editinterval + this.lastedit - dateNow;
                    if (timeToWait < 0)
                        timeToWait = 0;
                    if (this.options.logging) {
                        console.log(`last edit ${this.lastedit}`);
                        console.log(`date Now  ${dateNow}`);
                        console.log(`Interval ${dateNow - this.lastedit}`);
                        console.log(`Waiting ${timeToWait}`);
                    }
                    setTimeout(() => {
                        //
                        // After waiting, post the update
                        //
                        const cycleStart = Date.now();
                        const form = {
                            action: 'edit',
                            basetimestamp: page.timestamp,
                            title: page.title,
                            summary: page.editsummary,
                            text: page.text
                        };
                        this.updPrompt(page.title).then((input) => {
                            if ('abort' in input) {
                                reject(new Error('Aborted at user request'));
                                return;
                            }
                            if ('noprompt' in input)
                                this.options.prompt = false;
                            if ('maxlag' in input)
                                try {
                                    this.maxlag = parseInt(input.maxlag);
                                }
                                catch (e) {
                                    console.log(e.message);
                                }
                            if ('skip' in input) {
                                console.log('Skipped update...');
                                resolve();
                            }
                            else {
                                if (this.options.dryrun) {
                                    if (this.formBreaksActionLimits(form, reject))
                                        return;
                                    this.printPage(page);
                                    resolve();
                                }
                                else {
                                    this.request(form).then((r) => {
                                        if (this.options.logging) {
                                            console.log(JSON.stringify(r))
                                            console.log(`Elapsed interval = ${cycleStart - this.lastedit}`);
                                        }
                                        if (r.error)
                                            reject(r.error)
                                        else {
                                            console.log(`Updated at ${new Date()}`);
                                            this.lastedit = cycleStart; // Only set when successful
                                            resolve();
                                        }
                                    }).catch(reject);
                                }
                            }
                        }).catch(reject);
                    }, timeToWait);
                }
                else
                    resolve();
            }).catch(reject);
        });
    }
    //
    // Retrieve details of all the pages in a tranche
    //
    processTranche(titleTranche) {
        return new Promise((resolve, reject) => {
            if (this.instance.shutoffpage)
                titleTranche = titleTranche + '|' + this.instance.shutoffpage;
            this.request({
                action: 'query',
                prop: 'info|revisions',
                maxlag: this.maxlag,
                rvprop: 'timestamp|content',
                titles: titleTranche
            }).then((bodyObj) => {
                const pageArray = [];
                //
                // Push retrieved details into an array for processing
                //
                for (let page in bodyObj.query.pages) {
                    const thisPage = bodyObj.query.pages[page];
                    if (thisPage.title == this.instance.shutoffpage)
                        if (this.shutofftimestamp && thisPage.revisions && this.shutofftimestamp != thisPage.revisions[0].timestamp) {
                            reject(new Error(`Shutting down. Timestamp of shutoffpage ${this.instance.shutoffpage} changed to ${thisPage.revisions[0].timestamp}`));
                            return;
                        }
                        else
                            this.shutofftimestamp = thisPage.revisions ? thisPage.revisions[0].timestamp : 'NOSUCHPAGE';
                    else if (thisPage.revisions) {
                        const pageText = thisPage.revisions[0]['*'];
                        if (this.options.logging)
                            console.log('**Page=' + thisPage.title);
                        if (allowBots(pageText, this.instance.lgname))
                            pageArray.push({
                                title: thisPage.title,
                                timestamp: thisPage.revisions[0].timestamp,
                                text: pageText
                            });
                        else
                            console.log('Bot excluded from ' + thisPage.title); // But what if a module is just scanning a page? 
                    }
                    else if (this.options.create)
                        pageArray.push({
                            title: thisPage.title,
                        });
                    else
                        console.log('!!Missing page=' + thisPage.title); // A page with no revisions isn't there
                }
                //
                // Process array of pages
                //
                pageArray.sort((a, b) => { return (a.title > b.title) ? 1 : -1; });
                pageArray.reduce((promise, item) => {
                    return promise.then((result) => {
                        console.log("Processing: " + item.title);
                        return this.processPage(item);
                    });
                }, Promise.resolve())
                    .then(resolve)
                    .catch(reject);
            }).catch(reject);
        });
    }
    //
    // Process the tranches of page titles
    //
    runTranches() {
        return this.titleTranches.reduce((promise, item) => {
            return promise.then((result) => {
                if (this.options.logging)
                    console.log(`Processing tranche: ${item}`);
                return this.processTranche(item);
            });
        }, Promise.resolve());
    }
    run(params) {
        return new Promise((resolve, reject) => {
            this.initConfig(params)
                .then(() => {
                    return this.setTitleTranches();
                })
                .then(() => {
                    return this.setCookie();
                })
                .then(() => {
                    return this.runTranches();
                })
                .catch((e) => {
                    if (!this.options || this.options.logging)
                        console.error(e);
                    else
                        console.log(e.message);
                });
        });
    }
    wikiStream(params, highWaterMark) {
        return new _wikiStream(this, params, highWaterMark);
    }
    wikiGeneratorStream(params, maxcount, highWaterMark) {
        return new _wikiGeneratorStream(this, params, maxcount, highWaterMark);
    }
    petScanStream(params, highWaterMark) {
        return new _petScanStream(this, params, highWaterMark);
    }
    chunker(params, highWaterMark) {
        return new _chunker(this, params, highWaterMark);
    }
    exclusionFilter(highWaterMark) {
        return new _exclusionFilter(this, highWaterMark);
    }
    filter(params, highWaterMark) {
        return new _filter(this, params, highWaterMark);
    }
    mwparserfromhell(func, highWaterMark) {
        return new _mwparserfromhell(this, func, highWaterMark);
    }
    runFunction(func, highWaterMark) {
        return new _runFunction(this, func, highWaterMark);
    }
    domAdder(raw, highWaterMark) {
        return new _domAdder(this, raw, highWaterMark);
    }
    attributesFromDom(attr, selector, deletedom, highWaterMark) {
        return new _attributesFromDom(this, attr, selector, deletedom, highWaterMark);
    }
    pageRetriever(highWaterMark) {
        return new _pageRetriever(this, highWaterMark);
    }
    entityRetriever(highWaterMark) {
        return new _entityRetriever(this, highWaterMark);
    }
    linkedEntityRetriever(highWaterMark) {
        return new _linkedEntityRetriever(this, highWaterMark);
    }
    siteLinkSetter(params) {
        return new _siteLinkSetter(this, params);
    }
    processingMiser(highWaterMark) {
        return new _processingMiser(this, highWaterMark);
    }
}
exports.botObject = botObject;

// Stream helper classes


const Readable = require('stream').Readable
const Transform = require('stream').Transform
class _wikiStream extends Readable {
    constructor(bot, params, highWaterMark) {
        if (highWaterMark) console.log(`wikiStream highWaterMark = ${highWaterMark}`)
        super({ "objectMode": true, "highWaterMark": highWaterMark });
        this.queryRun = bot.request(params)
        this.continue = null;
    }
    _read() {
        this.queryRun.then(r => {
            const keys = Object.keys(r.query.pages)
            let pushVal = getDataValue(keys, r);
            this.push(pushVal)
        }, e => { console.error("WikiStream error " + e.toString()) })
    }
}

class _wikiGeneratorStream extends Readable {
    constructor(bot, params, maxcount, highWaterMark) {
        if (highWaterMark) console.log(`wikiStream highWaterMark = ${highWaterMark}`)
        super({ "objectMode": true, "highWaterMark": highWaterMark });
        this.bot = bot
        this.params = params
        this.maxcount = maxcount
        this.count = 0
        this.continue = null;
        this.queryStart = new Date()
        this.daemoninterval = this.params.daemoninterval ? parseInt(this.params.daemoninterval) : 0
        this.runQuery(0)
    }
    _read() {
        if (this.maxcount && this.count > this.maxcount)
            this.push(null)
        else
            this.queryRun.then(r => {
                let delay = 0;

                const keys = Object.keys(r.query.pages)
                let pushVal = keys.length ? getDataValue(keys, r) : null;
                // If returning last value, kick off any continuation
                if (keys.length < 2 && !r.continued) {
                    if (r.continue)
                        this.continue = r.continue
                    else
                        this.continue = null
                    Object.assign(this.params, this.continue)
                    if (this.daemoninterval && !this.continue) {
                        this.queryStart.setTime(this.queryStart.getTime() + this.daemoninterval)
                        delay = this.queryStart.getTime() - Date.now();
                        if (delay < 0) {
                            delay = 0
                            this.queryStart = new Date()
                        }
                        console.log(`Delay=${delay}`)
                    }


                    if (this.continue || this.daemoninterval) {
                        r.continued = true;
                        this.runQuery(delay);

                    }
                    this.continue = null
                }

                if (pushVal) {
                    this.count++
                    console.log('GeneratorStream pushing: ' + (pushVal.title ? pushVal.title : JSON.stringify(pushVal)))
                    this.push(pushVal)
                }
                if (!pushVal && this.daemoninterval || pushVal && keys.length == 1) {
                    console.log('pushed flushmarker')
                    this.push(this.bot.flushMarker)
                }
                else {

                }
            }, e => { console.error("wikiGeneratorStream error " + e.toString()) })
    }

    runQuery(delay) {
        this.queryRun = this.bot.request(this.params, delay);
    }
}
class _petScanStream extends Readable {
    constructor(bot, params, highWaterMark) {
        if (highWaterMark) console.log(`petScanStream highWaterMark = ${highWaterMark}`)
        super({ "objectMode": true, "highWaterMark": highWaterMark });
        this.queryRun = bot.petScan(params)
    }
    _read() {
        this.queryRun.then(r => {
            this.push(r.length ? {
                title: r.shift().title
            } : null)
        }, e => { console.error("PetScan error " + e.toString()) })
    }
}

class _chunker extends Transform {
    constructor(bot, limit, highWaterMark) {
        if (highWaterMark) console.log(`Chunker highWaterMark = ${highWaterMark}`)
        super({
            "objectMode": true, "highWaterMark": highWaterMark
        })
        this.bot = bot;
        this.limit = limit;
        this.buffer = []
    }
    _transform(data, encoding, done) {
        if (this.bot.options.logging)
            console.log('Chunker input ' + this.limit + ':' + (data.title ? data.title : JSON.stringify(data)))

        this.buffer.push(data)
        if (this.buffer.length == this.limit) {
            this.pushbuffer()
        }
        if (data == this.bot.flushMarker) {
            this.flush(done)
        }
        else
            done();
    }
    flush(done) {
        if (this.bot.options.logging || 1)
            console.log("Chunker flush:" + this.limit)
        if (this.buffer.length)
            this.pushbuffer()
        done()
    }
    pushbuffer() {
        let output = this.buffer.splice(0)
        if (this.bot.options.logging) {
            console.log('Chunker output:\n' + JSON.stringify(output))
        }
        this.push(output)
    }

}

class _filter extends Transform {
    constructor(bot, executable, highWaterMark) {
        if (highWaterMark) console.log(`Filter highWaterMark = ${highWaterMark}`)
        super({ "objectMode": true, "highWaterMark": highWaterMark });
        this.bot = bot
        this.executable = executable;
    }
    _transform(data, encoding, done) {
        //console.log(`Filter -` + JSON.stringify(data))
        if (this.bot.options.logging)
            console.log('Filter input:' + (data.title ? data.title : JSON.stringify(data)))
        if (data == this.bot.flushMarker || this.executable(data))
            this.push(data)
        done();
    };
}

class _exclusionFilter extends Transform {
    constructor(bot, highWaterMark) {
        if (highWaterMark) console.log(`Filter exclusionFilter = ${highWaterMark}`)
        super({ "objectMode": true, "highWaterMark": highWaterMark });
        this.bot = bot
    }
    _transform(data, encoding, done) {
        if (data == this.bot.flushMarker || allowBots(data.page.revisions[0]['*'], this.bot.instance.lgname))
            this.push(data)
        else
            console.log('Bot excluded from ' + data.page.title);
        done();
    };
}
class _processingMiser extends Transform {
    constructor(bot, highWaterMark) {
        if (highWaterMark) console.log(`processingMiser highWaterMark= ${highWaterMark}`)
        super({ "objectMode": true, "highWaterMark": highWaterMark });
        this.bot = bot
        this.oldPageList = {}
        this.newPageList = {}
    }
    _transform(data, encoding, done) {
        if (data == this.bot.flushMarker) {
            this.oldPageList = this.newPageList;
            this.newPageList = {};
            this.push(data)
        }
        else {
            this.newPageList[data.title] = data.page.timestamp
            if (this.oldPageList[data.title] && this.oldPageList[data.title] == data.page.timestamp) {
                 // console.log('Already processed ' + data.page.title);
            }
            else
                this.push(data)
        }
        done();
    };
}
class _domAdder extends Transform {
    constructor(bot, raw, highWaterMark) {
        if (highWaterMark) console.log(`domAdder highWaterMark = ${highWaterMark}`)
        super({ "objectMode": true, "highWaterMark": highWaterMark });
        this.bot = bot;
        this.raw = raw;
    }
    _transform(data, encoding, done) {
        if (this.bot.options.logging)
            console.log('domAdder input:' + (data.title ? data.title : JSON.stringify(data)))
        if (data == this.bot.flushMarker)
            done(null, data)
        else
            this.bot.getHTML(data.title).then((out) => {
                if (!this.raw) delete (out.raw);
                data.html = out
                done(null, data)
            })
    }
}
class _mwparserfromhell extends Transform {
    constructor(bot, pystring, highWaterMark) {
        if (highWaterMark) console.log(`mwparserfromhell highWaterMark = ${highWaterMark}`)
        super({ "objectMode": true, "highWaterMark": highWaterMark });
        const ps = require("python-shell")
        this.pyshell = new ps.PythonShell('pyserver.py');
        this.pystring = pystring;
        this.on('end', () => { this._sendEncoded("<<<<EOF"); console.debug("Python process shut down") })
    }
    _sendEncoded(python) {
        this.pyshell.send(encodeURIComponent(python))
    }
    _transform(data, encoding, done) {
        if (data == this.bot.flushMarker)
            done(null, data)
        else {
            this._sendEncoded(
                `wikicode=mwparserfromhell.parse(r"""${data.page.revisions[0]["*"]}""")`)
            this._sendEncoded(this.pystring(data))
            this._sendEncoded('returnValue(str(wikicode))')
            this.pyshell.once('message', (output) => {
                data.page.revisions[0]["*"] = decodeURIComponent(output)
                done(null, data)
            })
        }
    }
}
class _runFunction extends Transform {
    constructor(bot, func, highWaterMark) {
        if (highWaterMark) console.log(`runFunction highWaterMark = ${highWaterMark}`)
        super({ "objectMode": true, "highWaterMark": highWaterMark });
        this.bot = bot
        this.func = func
    }
    _transform(data, encoding, done) {
        if (data == this.bot.flushMarker)
            done(null, data)
        else
            this.func(data, () => { done(null, data) }, this.bot)
    }
}
class _attributesFromDom extends Transform {
    constructor(bot, attr, selector, deletedom, highWaterMark) {
        if (highWaterMark) console.log(`attributesFromDom highWaterMark = ${highWaterMark}`)
        super({ "objectMode": true, "highWaterMark": highWaterMark });
        this.bot = bot
        this.attr = attr;
        this.selector = selector;
        this.deletedom = deletedom
    }
    _transform(data, encoding, done) {
        if (data != this.bot.flushMarker) {
            const sOut = data.html.$(this.selector)
            if (sOut.length)
                data[this.attr] = data.html.$.trim(sOut.text())
            else
                data[this.attr] = null
            if (this.deletedom) delete (data.html.dom); // Big. Get rid.
        }
        done(null, data)
    }
}
class _entityRetriever extends Transform {
    constructor(bot, highWaterMark) {
        if (highWaterMark) console.log(`entityRetriever highWaterMark = ${highWaterMark}`)
        super({ "objectMode": true, "highWaterMark": highWaterMark });
        this.bot = bot;
        this.flushrec = false;
    }
    _transform(data, encoding, done) {
        if (data[data.length - 1] == this.bot.flushMarker) {
            this.flushrec = data.pop()
        }
        const entitylist = data.filter(p => p.wikidatakey).map(p => p.wikidatakey).join('|')
        if (this.bot.options.logging)
            console.log(`entitylist='${entitylist}'`)
        if (entitylist == '')
            this.flush(done);
        else
            this.bot.dataRequest({
                action: 'wbgetentities',
                ids: entitylist
            }).then((results) => {
                const ents = results.entities
                Object.keys(data).forEach(dk => {
                    //console.log('dk=' + dk)
                    if (ents[data[dk].wikidatakey]) {
                        data[dk].entity = ents[data[dk].wikidatakey]
                    }
                    this.push(data[dk])
                })
                this.flush(done);
            })
    }
    flush(done) {
        if (this.flushrec)
            this.push(this.flushrec)
        done()
    }
}
class _pageRetriever extends Transform {
    constructor(bot, highWaterMark) {
        if (highWaterMark) console.log(`pageRetriever highWaterMark = ${highWaterMark}`)
        super({ "objectMode": true, "highWaterMark": highWaterMark });
        this.bot = bot;
    }
    _transform(data, encoding, done) {
        if (data == this.bot.flushMarker)
            done(null, data)
        else {
            const titlelist = data.map(p => p.title).join('|')
            if (this.bot.options.logging)
                console.log('titlelist=' + titlelist)
            if (titlelist == '')
                done();
            else
                this.bot.request({
                    action: 'query',
                    prop: 'info|revisions',
                    rvprop: 'timestamp|content',
                    titles: titlelist
                }).then((r) => {

                    while (Object.keys(r.query.pages).length) {
                        let pushVal = getDataValue(Object.keys(r.query.pages), r);
                        let dataitem = data.find((d) => d.title == pushVal.title)
                        pushVal = Object.assign(dataitem, pushVal)
                        this.push(pushVal)
                    }
                    done()
                })
        }
    }
}
class _linkedEntityRetriever extends Transform {
    constructor(bot, highWaterMark) {
        if (highWaterMark) console.log(`linkedEntityRetriever highWaterMark = ${highWaterMark}`)
        super({ "objectMode": true, "highWaterMark": highWaterMark });
        this.bot = bot;
        this.flushref = false;
    }
    _transform(data, encoding, done) {
        if (data[data.length - 1] == this.bot.flushMarker) {
            this.flushrec = data.pop()
        }
        const titlelist = data.map(p => p.title).join('|')
        if (this.bot.options.logging)
            console.log('titlelist=' + titlelist)
        if (titlelist == '')
            this.flush(done);
        else
            this.bot.dataRequest({
                action: 'wbgetentities',
                sites: 'enwiki',
                titles: titlelist
            }).then((results) => {
                const ents = results.entities
                Object.keys(ents).forEach(dk => {
                    if (ents[dk].sitelinks) {
                        const title = ents[dk].sitelinks.enwiki.title //.replace(/ /g, '_')
                        const page = data.find((page) => page.title == title)
                        page.currEntity = dk
                        //if (this.bot.options.logging)
                        console.log(title + ' is already linked to ' + dk);
                    }
                })
                Object.keys(data).forEach((dk) => {
                    this.push(data[dk])
                })
                this.flush(done)
            })
    }
    flush(done) {
        if (this.flushrec) {
            this.push(this.flushrec)
            this.flushrec = false
        }
        done()
    }
}
class _siteLinkSetter extends Transform {
    constructor(bot, params) {
        params = params ? params : {}
        if (params.highWaterMark) console.log(`siteLinkSetterhighWaterMark = ${params.highWaterMark}`)
        super({ "objectMode": true, "highWaterMark": params.highWaterMark });
        this.bot = bot;
        this.nextEditTime = 0;
        this.counter = params.limit ? params.limit : Number.MAX_SAFE_INTEGER
        this.dryrun = params.dryrun;
    }
    _transform(data, encoding, done) {
        if (data == this.bot.flushMarker)
            done(null, data)
        else {
            if (!this.counter--) process.exit()
            let now = new Date().getTime()
            let wait = this.nextEditTime - now
            if (now > this.nextEditTime)
                this.nextEditTime = now
            this.nextEditTime += 10000
            if (wait < 0)
                wait = 0;
            console.log('next edit:' + new Date(this.nextEditTime).toString() + ' wait:' + wait.toString())
            setTimeout(() => {
                if (this.dryrun) {
                    done(null, {
                        title: data.title,
                        datakey: data.wikidatakey,
                        result: 'dryrun'
                    })
                } else {
                    this.bot.dataRequest({
                        action: 'wbsetsitelink',
                        id: data.wikidatakey,
                        linktitle: data.title,
                        linksite: 'enwiki',
                    }).then((r) => {
                        done(null, {
                            title: data.title,
                            datakey: data.wikidatakey,
                            result: r
                        })
                    })
                        .catch((e) => {
                            this.nextEditTime = 0
                            done(null, {
                                title: data.title,
                                datakey: data.wikidatakey,
                                result: e
                            })
                        })
                }
            }, wait)
        }
    }
}

function getDataValue(keys, r) {
    let pushVal = null
    if (keys.length) {
        let key = keys[0];
        pushVal = { page: r.query.pages[key] }
        if (pushVal.page.revisions)
            pushVal.page.timestamp = pushVal.page.revisions[0].timestamp;
        pushVal.title = pushVal.page.title;
        delete (r.query.pages[key]);
    }
    return pushVal;
}

