import sys
import mwparserfromhell
import urllib.parse

def returnValue(value):
    print(urllib.parse.quote(value))

for line in sys.stdin:
    text = urllib.parse.unquote(line)
    if '<<<<EOF' == text.rstrip():
        break
    exec(text)
    sys.stdout.flush()
