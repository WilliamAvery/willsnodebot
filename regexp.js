exports.run = function(page, options) {
    return new Promise((accept, reject) => {
        console.log('regexp: ' + page.title + ' timestamp: ' + page.timestamp)
        const regexp = new RegExp(options.regexp, options.flags)
        var newText = page.text.replace(regexp, options.subst)
        if (newText !== page.text) {
            page.editsummary = options.summary ? options.summary : "Updating"
            page.text = newText
        }
        accept(page)
    })
};
