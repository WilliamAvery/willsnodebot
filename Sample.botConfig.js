exports.settings = {
	cookieJar: './.botCookies.txt',
	petscanUri: 'https://petscan.wmflabs.org/',
	maxlag: 3,
	continuable_errors: ['maxlag', 'editconflict'],
	instances: {
    		'live': {
    		Description: 'Live environment',
    		ApiUrl: 'https://en.wikipedia.org/w/api.php',
		lgname: 'William Avery Bot',
		assert: 'bot',
		shutoffpage: ':',
		tranchesize: 100,
		editinterval: 10000
		},
		// This instance is used with taxonbar_syncerbot.js
		'data': {
    		Description: 'Data environment',
                BaseUrl: 'https://en.wikipedia.org/wiki/',
    		ApiUrl: 'https://en.wikipedia.org/w/api.php',
                DataUrl: 'https://www.wikidata.org/w/api.php',
		lgname: 'William Avery Bot',
		assert: 'user'
		},
    		'test': {
	    	Description: 'Test environment',
    		ApiUrl: 'https://test.wikipedia.org/w/api.php',
		lgname: 'William Avery',
		//assert: 'bot',
		shutoffpage: ':',
		tranchesize: 5,
		editinterval:  2000
		}
	}
};
