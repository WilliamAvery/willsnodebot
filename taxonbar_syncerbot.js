const WillsNodeBot = require('./willsnodebot').botObject
//
// Bot - framework handles authentication,
// maxlag retries, transient network errors
//


const taxonbar_syncerbot_interval = process.env.taxonbar_syncerbot_interval
console.log("taxonbar_syncerbot_interval=" + taxonbar_syncerbot_interval)
let runStartTime = new Date().getTime()
let bot_wait = 0

function runBot() {

    setTimeout(function () {
        const bot = new WillsNodeBot()

        bot.initConfig({
            instance: 'data',
            'logging': false
        })

        bot.setCookie().then(() => {
            bot.wikiGeneratorStream({
                action: 'query',
                generator: 'categorymembers',
                gcmtitle: 'Category:Taxonbars_desynced_from_Wikidata',
                gcmlimit: 5000,    // gcmendsortkeyprefix only seems to work in the absence of continuation
                prop: 'revisions',
                rvprop: 'timestamp',
                daemoninterval: 1200000
            })
                .pipe(bot.processingMiser(3))
                //
                // Group output for single API request
                //
                .pipe(bot.chunker(50))
                //
                // Check for currently linked entities
                //
                .pipe(bot.linkedEntityRetriever())
                .pipe(bot.filter(page => !page.currEntity))
                //
                // Get HTML for Wikipedia page
                // and attach DOM obj with jQuery
                //
                .pipe(bot.domAdder(false))
                //
                // Get wikidata key from taxonbar
                //
                .pipe(bot.attributesFromDom("wikidatakey", "div.navbox a[title^=wikidata].extiw:first"))
                .pipe(bot.filter(d => d.wikidatakey)) // Discard ones that have a blank taxonbar :-(
                //
                // Group output for API request to retrieve the entities referenced in the taxonbars
                //
                .pipe(bot.chunker(20))
                .pipe(bot.entityRetriever())
                //
                // Filter out any where there the entity either doesn't exist or already has enwiki link
                //
                .pipe(bot.filter((d) => d.entity && !d.entity.sitelinks.enwiki))
                //
                // Set site link , or just print if dry run
                //
                .pipe(bot.siteLinkSetter({
                    dryrun: false,
                    limit: 2000
                }))
                .on('data', (obj) => {
                    console.log('setsitelink result: ' + JSON.stringify(obj))
                })
                .on('end', () => {
                    let runEndTime = new Date().getTime()
                    const diff = runEndTime - runStartTime
                    if (taxonbar_syncerbot_interval) {
                        bot_wait = taxonbar_syncerbot_interval - diff
                        if (bot_wait < 0)
                            bot_wait = 0
                        runStartTime = runEndTime + bot_wait
                        console.log(`Bot daemon will wait ${bot_wait} ms`)
                        runBot()
                    }
                    else
                    console.log(`Not in daemon mode`)
                })
        })
    }, bot_wait)
}
runBot() 
