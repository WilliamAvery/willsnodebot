"use strict";

exports.run = function(page, options, services) {
    return new Promise((accept, reject) => {
        console.log('wikiProjectDesynced: ' + page.title + ' timestamp: ' + page.timestamp)
        // PetScan to get pages with taxonbar desynced
        // and appropriate WikiProject template on talk page
        const projectTemplate = 'WikiProject ' + page.title.substr(options.basepage.length)
        services.petscan({
            categories: 'Taxonbars_desynced_from_Wikidata',
            templates_yes: projectTemplate,
            templates_use_talk_yes: 'on',
        }).then(
            (pages) => {
                const jsDate = new Date()
                const curTimeStamp = services.PetDate(jsDate)
                console.log('timestamp=' + curTimeStamp)
                let preamble = '{|class="wikitable sortable"\n!Page!!Entered list!!Note!!Resolved'
                let postamble = '\n|}'
                let bodyText = ''
                const tableData = {}
                if (page.text) {
                    let tableStart = page.text.indexOf(preamble)
                    preamble = page.text.substr(0, tableStart) + preamble
                    let postambleStart = page.text.indexOf(postamble)
                    postamble = page.text.substr(postambleStart)
                    bodyText = page.text.substr(preamble.length, page.text.length - preamble.length - postamble.length)
                    const tableRows = bodyText.split(/\n+\|\-\n+/)
                    tableRows.forEach((tableRow) => {
                        let tableCells = tableRow.split('||')
                        if (tableCells.length > 1) {
                            let pageTitle = tableCells[0]
                            pageTitle = pageTitle.substr(3, pageTitle.length - 5)
                            let touched = services.IsoToPet(tableCells[1])
                            let note = tableCells[2]
                            let resolved = services.IsoToPet(tableCells[3])
                            tableData[pageTitle] = {
                                touched: touched,
                                note: note,
                                resolved: resolved ? resolved : curTimeStamp // Mark unresolved as resolved now
                            }
                        }
                    })
                }
                // merge petscan results into existing data
                pages.forEach((page) => {
                    let pageTitle = page.title.replace(/_/g, ' ')
                    if (pageTitle in tableData) // Existing table row
                        tableData[pageTitle].resolved = '' // Not resolved, as found in petscan results
                    else // New
                        tableData[pageTitle] = {
                            touched: curTimeStamp, // Not page's timestamp: desync might be due to wikidata edit
                            note: '',
                            resolved: ''
                        }
                })
                console.log(JSON.stringify(tableData))
                let dataArray = Array.from(Object.keys(tableData), (key) => {
                    tableData[key]['title'] = key;
                    return tableData[key]
                })
                dataArray.sort((e1, e2) => {
                    let retVal = 0;
                    if (e1.resolved.trim() || e2.resolved.trim())
                        if (e1.resolved != e2.resolved)
                            if (!(e1.resolved && e2.resolved)) // Not both resolved
                                retVal = e1.resolved > e2.resolved ? 1 : -1 // Unresolved before resolved
                    else
                        retVal = e1.resolved < e2.resolved ? 1 : -1 // Put latest resolved first
                    else
                        retVal = e1.title > e2.title ? 1 : -1
                    else if (e1.touched != e2.touched)
                        retVal = e1.touched < e2.touched ? 1 : -1
                    else
                        retVal = e1.title > e2.title ? 1 : -1
                    return retVal
                })
                // discard long-resolved rows at end of array
                const discardTimeStamp = services.PetDate(new Date(jsDate - 24 * 3600000))
                let dRow = dataArray.length - 1
                while (dRow > -1 && dataArray[dRow].resolved && dataArray[dRow].resolved < discardTimeStamp) {
                    console.log(dataArray[dRow].title + ': ' + dataArray[dRow].resolved + ' < ' + discardTimeStamp + ' - discarding')
                    dataArray.pop()
                    dRow--
                }
                let tableBody = dataArray.map(page => `\n|-\n|[[${page.title}]]||${services.PetToIso(page.touched)}|| ${page.note.trim()} ||${services.PetToIso(page.resolved)}`).join('')
                const newPageText = preamble + tableBody + postamble
                if (newPageText !== page.text) {
                    page.text = newPageText
                    page.editsummary = options.summary ? options.summary : page.timestamp ? "Updating" : "Creating"
                }
                accept(page)
            }
        ).catch((e) => {
            reject(e)
        })
    })
}